locals {
  merge_tags       = merge(var.datadog_tags, data.aws_default_tags.provider.tags)
  datadog_tags     = [for key, value in local.merge_tags : "${key}:${value}"]
  all_regions      = data.aws_regions.all.names
  excluded_regions = setsubtract(local.all_regions, var.include_regions)
}

resource "datadog_integration_aws" "integration" {
  count = var.enabled ? 1 : 0

  account_id       = var.account_id
  excluded_regions = local.excluded_regions
  role_name        = "DatadogAWSIntegrationRole"

  host_tags = flatten(concat([
    [format("Account_ID:%s", var.account_id), "Infra:AWS"],
    local.datadog_tags
  ]))

  account_specific_namespace_rules = var.account_specific_datadog_namespace_rules
  # https://docs.datadoghq.com/integrations/amazon_web_services/#cloud-security-posture-management
  cspm_resource_collection_enabled = true # reqires IAM policy assigned to DD role
  metrics_collection_enabled       = true
  resource_collection_enabled      = true
}
