variable "enabled" {
  type        = bool
  description = "Toggles creation of resources and data sources in this repository"
  default     = true
}

variable "account_id" {
  type        = string
  description = "AWS account id"
}

variable "region" {
  type        = string
  description = "AWS region"
}

variable "include_regions" {
  type        = list(string)
  description = "An array of AWS regions to exclude from metrics collection."
  default = [
    "eu-west-2",
    "us-east-1",
  ]
}

variable "account_specific_datadog_namespace_rules" {
  type        = map(bool)
  description = "Enables or disables metric collection for specific AWS namespaces for this AWS account only"
}

variable "tags" {
  type        = map(any)
  default     = {}
  description = "(optional) Override tags to apply to AWS resources associated with the Datadog integration"
}

variable "datadog_tags" {
  type        = map(any)
  default     = {}
  description = "(optional) additional Tags to apply to the Datadog integration"
}
